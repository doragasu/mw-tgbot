/************************************************************************//**
 * \brief Telegram bot PoC
 *
 * Read README.md for setup instructions.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2019
 *
 * \note Server certificate validation is NOT required. If you do not set
 * a certificate (e.g. by calling mw_cert_set(0, NULL, )), server cert will
 * not be validated. This can save some time and resources, but opens
 * client to a MitM attack.
 *
 * \defgroup 1985ch main
 * \{
 ****************************************************************************/
#include <string.h>
#include <stdio.h>
#include "vdp.h"
#include "mw/util.h"
#include "mw/mpool.h"
#include "mw/megawifi.h"
#include "mw/loop.h"
#include "mw/json.h"
#include "tg_token.h"
#include "tg_bot.h"

#ifndef TGB_API_TOKEN
#error "Missing API token. Create tg_token.h and put your token there." \
" See README.md for details. Make sure to keep your token safe!"
#endif

/// Length of the receive message buffer. It can be way slower if you do not
/// want to support receiving chains of updates.
#define MW_MSG_BUFLEN	16384

/// Length of the general purpose buffer.
#define MW_CMD_BUFLEN	1460

/// TCP port to use (set to Megadrive release year ;-)
#define MW_CH_PORT 	1985

/// 1024 tokens should take 8 KiB RAM
#define MW_JSON_TOKEN_MAX 1024

/// Maximum number of loop functions
#define MW_MAX_LOOP_FUNCS	2

/// Maximun number of loop timers
#define MW_MAX_LOOP_TIMERS	4

/// General purpose command buffer
static char cmd_buf[MW_CMD_BUFLEN];

/// Message reception buffer
static char msg_buf[MW_MSG_BUFLEN];

static jsmntok_t json_tok[MW_JSON_TOKEN_MAX] = {};

/// Certificate for api.telegram.org
static const char cert[] = "-----BEGIN CERTIFICATE-----\n"
"MIIE0DCCA7igAwIBAgIBBzANBgkqhkiG9w0BAQsFADCBgzELMAkGA1UEBhMCVVMx"
"EDAOBgNVBAgTB0FyaXpvbmExEzARBgNVBAcTClNjb3R0c2RhbGUxGjAYBgNVBAoT"
"EUdvRGFkZHkuY29tLCBJbmMuMTEwLwYDVQQDEyhHbyBEYWRkeSBSb290IENlcnRp"
"ZmljYXRlIEF1dGhvcml0eSAtIEcyMB4XDTExMDUwMzA3MDAwMFoXDTMxMDUwMzA3"
"MDAwMFowgbQxCzAJBgNVBAYTAlVTMRAwDgYDVQQIEwdBcml6b25hMRMwEQYDVQQH"
"EwpTY290dHNkYWxlMRowGAYDVQQKExFHb0RhZGR5LmNvbSwgSW5jLjEtMCsGA1UE"
"CxMkaHR0cDovL2NlcnRzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkvMTMwMQYDVQQD"
"EypHbyBEYWRkeSBTZWN1cmUgQ2VydGlmaWNhdGUgQXV0aG9yaXR5IC0gRzIwggEi"
"MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC54MsQ1K92vdSTYuswZLiBCGzD"
"BNliF44v/z5lz4/OYuY8UhzaFkVLVat4a2ODYpDOD2lsmcgaFItMzEUz6ojcnqOv"
"K/6AYZ15V8TPLvQ/MDxdR/yaFrzDN5ZBUY4RS1T4KL7QjL7wMDge87Am+GZHY23e"
"cSZHjzhHU9FGHbTj3ADqRay9vHHZqm8A29vNMDp5T19MR/gd71vCxJ1gO7GyQ5HY"
"pDNO6rPWJ0+tJYqlxvTV0KaudAVkV4i1RFXULSo6Pvi4vekyCgKUZMQWOlDxSq7n"
"eTOvDCAHf+jfBDnCaQJsY1L6d8EbyHSHyLmTGFBUNUtpTrw700kuH9zB0lL7AgMB"
"AAGjggEaMIIBFjAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBBjAdBgNV"
"HQ4EFgQUQMK9J47MNIMwojPX+2yz8LQsgM4wHwYDVR0jBBgwFoAUOpqFBxBnKLbv"
"9r0FQW4gwZTaD94wNAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzABhhhodHRwOi8v"
"b2NzcC5nb2RhZGR5LmNvbS8wNQYDVR0fBC4wLDAqoCigJoYkaHR0cDovL2NybC5n"
"b2RhZGR5LmNvbS9nZHJvb3QtZzIuY3JsMEYGA1UdIAQ/MD0wOwYEVR0gADAzMDEG"
"CCsGAQUFBwIBFiVodHRwczovL2NlcnRzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkv"
"MA0GCSqGSIb3DQEBCwUAA4IBAQAIfmyTEMg4uJapkEv/oV9PBO9sPpyIBslQj6Zz"
"91cxG7685C/b+LrTW+C05+Z5Yg4MotdqY3MxtfWoSKQ7CC2iXZDXtHwlTxFWMMS2"
"RJ17LJ3lXubvDGGqv+QqG+6EnriDfcFDzkSnE3ANkR/0yBOtg2DZ2HKocyQetawi"
"DsoXiWJYRBuriSUBAA/NxBti21G00w9RKpv0vHP8ds42pM3Z2Czqrpv1KrKQ0U11"
"GIo/ikGQI31bS/6kA1ibRrLDYGCD+H1QQc7CoZDDu+8CL9IVVO5EFdkKrqeKM+2x"
"LXY2JtwE65/3YR8V3Idv7kaWKK2hJn0KCacuBKONvPi8BDAB\n"
"-----END CERTIFICATE-----";
static const uint16_t cert_len = sizeof(cert);
/// Certificate hash, obtained with command:
/// openssl x509 -hash -in <cert_file_name> -noout
static const uint32_t cert_hash = 0x27eb7704;

static void println(const char *str, int color)
{
	static unsigned int line = 2;

	if (str) {
		VdpDrawText(VDP_PLANEA_ADDR, 2, line, color, 36, str, 0);
	}
	line++;
}

static void idle_cb(struct loop_func *f)
{
	UNUSED_PARAM(f);
	mw_process();
}

static void http_cert_set(void)
{
	// Remove cert and change code below to mw_cert_set(0, NULL, 0) if
	// you want to be on the unsafe side and not validate cerver cert.
	uint32_t hash = mw_http_cert_query();
	if (hash != cert_hash) {
		mw_http_cert_set(cert_hash, cert, cert_len);
	}
}

static enum mw_err get_me(void)
{
	char username[1024] = "bot name: @";
	const char *item;
	enum mw_err err;
	unsigned int len;
	jsmn_parser parser;
	int tokens;
	int parent;
	int i;

	err = tgb_get("getMe", msg_buf, &len);
	if (err) {
		goto err;
	}
	jsmn_init(&parser);
	tokens = jsmn_parse(&parser, msg_buf, len, json_tok,
			MW_JSON_TOKEN_MAX);

	if (tokens < 0) {
		err = -1;
		goto err;
	}
	json_null_terminate(msg_buf, json_tok, tokens);
	if (!tgb_ok_check(msg_buf, json_tok, tokens)) {
		err = -1;
		goto err;
	}
	parent = json_key_search("result", msg_buf, json_tok, 1, 0, tokens);
	if (parent < 0) {
		err = -1;
		goto err;
	}
	i = json_key_search("username", msg_buf, json_tok, parent + 1,
			parent, tokens);
	if (parent < 0) {
		err = -1;
		goto err;
	}
	item = json_item(msg_buf, json_tok, i);
	if (item) {
		strcpy(&username[11], item);
	}
	println(username, VDP_TXT_COL_CYAN);

err:
	return err;
}

static void get_user(const char **id, const char **username,
		const char *json_str, const jsmntok_t *json_tok,
		int parent, int num_tok)
{
	int i;

	i = json_key_search("from", json_str, json_tok, parent + 1,
			parent, num_tok);
	if (i < 0) {
		return;
	}

	parent = i;
	i = json_key_search("id", json_str, json_tok, parent + 1,
			parent, num_tok);
	*id = json_item(json_str, json_tok, i);
	i = json_key_search("username", json_str, json_tok, parent + 1,
			parent, num_tok);
	*username = json_item(json_str, json_tok, i);
}

static void print_msg(const char *username, const char *msg)
{
	char line[40];
	int i = 0;
	int len = 0;

	if (username) {
		i = strlen(username);
		i = MIN(i, 16);
		memcpy(line, username, i);
	}
	line[i++] = ':';
	line[i++] = ' ';
	if (msg) {
		len = strlen(msg);
		len = MIN(39 - i, len);
		memcpy(&line[i], msg, len);
	}
	line[i + len] = '\0';

	println(line, VDP_TXT_COL_WHITE);
}

static int parse_update(const char *json_str, const jsmntok_t *json_tok,
		int idx, int num_tok, char *offset)
{
	const char *username = NULL;
	const char *id = NULL;
	const char *msg;
	enum mw_err err = -1;
	int parent = idx;
	int i = parent + 1;

	i = json_key_search("update_id", json_str, json_tok, i,
			parent, num_tok);
	if (i < 0) {
		goto out;
	}

	// Even if we do not have more fields, this is not an error
	// (this might be e.g. a video update)
	err = 0;
	strcpy(offset, &json_str[json_tok[i].start]);
	i = json_key_search("message", json_str, json_tok, parent + 1,
			parent, num_tok);
	if (i < 0) {
		goto out;
	}

	parent = i;
	i = json_key_search("text", json_str, json_tok, parent + 1,
			parent, num_tok);
	if (i < 0) {
		goto out;
	}

	get_user(&id, &username, json_str, json_tok, parent, num_tok);
	msg = json_item(json_str, json_tok, i);
	msg = strcasestr(msg, "Genesis does") ? "What Nintendon't "
		"\\ud83d\\ude02" : msg;
	tgb_msg_send(id, msg);
	print_msg(username, msg);

out:
	return err;
}

static int str_inc(char *num)
{
	int len = strlen(num);
	int digit = len;
	bool carry;

	if (!len) {
		goto out;
	}

	do {
		carry = false;
		digit--;
		if (digit < 0) {
			for (int i = len; i > 0; i--) {
				num[i] = num[i - 1];
			}
			len++;
			num[len] = '\0';
			num[0] = '1';
		} else {
			num[digit]++;
			if (num[digit] > '9') {
				num[digit] = '0';
				carry = true;
			}
		}
	} while (carry);

out:
	return len;
}

#define UPDATES_METHOD	"getUpdates?allowed_updates=messages&offset="
static enum mw_err process_updates(char *offset, const char *timeout_s)
{
	char method[256] = UPDATES_METHOD;
	int off = sizeof(UPDATES_METHOD) - 1;
	enum mw_err err;
	unsigned int len;
	jsmn_parser parser;
	int tokens;
	int array;
	int i;

	len = strlen(offset);
	memcpy(method + off, offset, len);
	off += len;
	len = sizeof("&timeout=") - 1;
	memcpy(method + off, "&timeout=", len);
	off += len;
	strcpy(method + off, timeout_s);
	bool updated = false;

	err = tgb_get(method, msg_buf, &len);
	if (err) {
		err = -1;
		goto out;
	}

	err = -1;
	jsmn_init(&parser);
	tokens = jsmn_parse(&parser, msg_buf, len, json_tok, MW_JSON_TOKEN_MAX);
	if (tokens < 0) {
		goto out;
	}

	json_null_terminate(msg_buf, json_tok, tokens);
	if (!tgb_ok_check(msg_buf, json_tok, tokens)) {
		goto out;
	}
	array = json_key_search("result", msg_buf, json_tok, 1, 0, tokens);
	if (array < 0 || json_tok[array].type != JSMN_ARRAY) {
		goto out;
	}

	i = array + 1;
	do {
		err = parse_update(msg_buf, json_tok, i, tokens, offset);
		if (!err) {
			i = json_object_next(json_tok, i, array, tokens);
			updated = true;
		}
	} while (!err && i > 0);

	if (updated) {
		str_inc(offset);
	}
	err = 0;

out:
	return err;
}

static void run_bot(struct loop_timer *t)
{
	enum mw_err err;
	char offset[16] = "0";

	// Join AP
	println("Associating to AP", VDP_TXT_COL_WHITE);
	err = mw_ap_assoc(0);
	if (err != MW_ERR_NONE) {
		goto err;
	}
	err = mw_ap_assoc_wait(MS_TO_FRAMES(30000));
	if (err != MW_ERR_NONE) {
		goto err;
	}
	println("DONE!", VDP_TXT_COL_CYAN);
	println(NULL, 0);

	http_cert_set();

	err = get_me();

	while (!err) {
		err = process_updates(offset, "60");
	}

err:
	println("ERROR!", VDP_TXT_COL_MAGENTA);
	mw_ap_disassoc();
	loop_timer_del(t);
}

/// MegaWiFi initialization
static void megawifi_init_cb(struct loop_timer  *t)
{
	uint8_t ver_major = 0, ver_minor = 0;
	char *variant = NULL;
	enum mw_err err;
	char line[] = "MegaWiFi version X.Y";

	// Try detecting the module
	err = mw_detect(&ver_major, &ver_minor, &variant);

	if (MW_ERR_NONE != err) {
		// Megawifi not found
		println("MegaWiFi not found!", VDP_TXT_COL_MAGENTA);
	} else {
		// Megawifi found
		line[17] = ver_major + '0';
		line[19] = ver_minor + '0';
		println(line, VDP_TXT_COL_WHITE);
		// Configuration complete, run test function next frame
		t->timer_cb = run_bot;
		loop_timer_start(t, 1);

	}
}

/// Loop run while idle
static void main_loop_init(void)
{
	// Run next frame, do not auto-reload
	static struct loop_timer frame_timer = {
		.timer_cb = megawifi_init_cb,
		.frames = 1
	};
	static struct loop_func megawifi_loop = {
		.func_cb = idle_cb
	};

	loop_init(MW_MAX_LOOP_FUNCS, MW_MAX_LOOP_TIMERS);
	loop_timer_add(&frame_timer);
	loop_func_add(&megawifi_loop);
}

/// Global initialization
static void init(void)
{
	// Initialize memory pool
	mp_init(0);
	// Initialize VDP
	VdpInit();
	// Initialize game loop
	main_loop_init();
	// Initialize MegaWiFi
	mw_init(cmd_buf, MW_CMD_BUFLEN);
	// Initialize telegram bot
	tgb_init(cmd_buf, msg_buf, MW_CMD_BUFLEN, MW_MSG_BUFLEN, TGB_API_TOKEN);
}

/// Entry point
int main(void)
{
	init();

	// Enter game loop (should never return)
	loop();

	return 0;
}

/** \} */

