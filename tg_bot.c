#include <string.h>
#include "mw/megawifi.h"
#include "tg_bot.h"
#include "mw/json.h"
#include "mw/loop.h"

#define TGB_REQ_MAX		256

#define TGB_ENDPOINT		"https://api.telegram.org/bot"
#define TGB_ENDPOINT_LEN	(sizeof(TGB_ENDPOINT) - 1)

struct tg_bot {
	char req[TGB_REQ_MAX];		///< Request buffer
	char *method;			///< Pointer to the API method start in req
	char *cmd_buf;			///< Command buffer for requests
	char *msg_buf;			///< Message buf for message reception
	unsigned int cmd_buf_len;	///< size of cmd_buf in bytes
	unsigned int msg_buf_len;	///< Size of msg_buf in bytes
};

static struct tg_bot b = {};

int tgb_init(char *cmd_buf, char *msg_buf, unsigned int cmd_buf_len,
		int msg_buf_len, const char *api_token)
{
	size_t len;

	// Prepare request buffer with the initial part:
	// "https://api.telegram.org/bot<BOT_API_TOKEN>/"
	memcpy(b.req, TGB_ENDPOINT, TGB_ENDPOINT_LEN);
	len = strlen(api_token);
	memcpy(b.req + TGB_ENDPOINT_LEN, api_token, len);
	len += TGB_ENDPOINT_LEN;
	b.req[len++] = '/';
	b.method = &b.req[len];
	b.cmd_buf = cmd_buf;
	b.cmd_buf_len = cmd_buf_len;
	b.msg_buf = msg_buf;
	b.msg_buf_len = msg_buf_len;

	return 0;
}

// Tries to synchronously receive exactly the indicated data length
static int sync_recv(uint8_t ch, char *buf, int len, uint16_t tout_frames)
{
	int recvd = 0;
	int err = 0;
	uint8_t get_ch = ch;
	int16_t get_len;

	while (err == 0 && recvd < len) {
		get_len = len - recvd;
		err = mw_recv_sync(&get_ch, buf + recvd, &get_len, tout_frames);
		if (!err) {
			if (get_ch != ch) {
				err = -1;
			} else {
				recvd += get_len;
			}
		}
	}

	return err;
}

static int tgb_begin(enum mw_http_method type, const char *method,
		unsigned int len) {
	enum mw_err err;

	strcpy(b.method, method);
	err = mw_http_url_set(b.req);
	if (!err) {
		err = mw_http_method_set(type);
	}
	if (!err) {
		err = mw_http_open(len);
	}

	return err;
}

static int tgb_finish(char *recv_buf, unsigned int *len)
{
	enum mw_err err;
	uint32_t content_len = 0;

	err = mw_http_finish(&content_len, MS_TO_FRAMES(60000));
	err = err >= 200 && err <= 300 ? 0 : err;
	if (content_len > b.msg_buf_len) {
		err = -1;
		// TODO: FIXME - We should flush Telegram buffer, or the bot
		// will not be able to progress anymore.
	}
	if (!err && content_len) {
		err = sync_recv(MW_HTTP_CH, recv_buf, content_len, 0);
	}
	if (!err && len) {
		*len = content_len;
	}

	return err;
}

int tgb_get(const char *method, char *recv_buf, unsigned int *len)
{
	enum mw_err err;

	err = tgb_begin(MW_HTTP_METHOD_GET, method, 0);
	if (!err) {
		err = tgb_finish(recv_buf, len);
	}

	return err;
}

int tgb_post(const char *method, const char *data, int length,
		const char *content_type, char *recv_buf,
		unsigned int *recv_len)
{
	enum mw_err err;

	// If this function fails, it is not critical
	mw_http_header_add("Content-Type", content_type);

	err = tgb_begin(MW_HTTP_METHOD_POST, method, length);
	if (!err) {
		// TODO Check timeout
		err = mw_send_sync(MW_HTTP_CH, data, length, 0);
	}
	if (!err) {
		err = tgb_finish(recv_buf, recv_len);
	}

	return err;
}

bool tgb_ok_check(const char *reply, const jsmntok_t *json_tok, int num_tokens)
{
	int i;

	i = json_key_search("ok", reply, json_tok, 1, 0, num_tokens);

	if (i < 0) {
		return false;
	}

	switch (json_bool_get(reply, json_tok, i)) {
		case 1:
			return true;

		default:
			return false;
	}
}

static int str_copy_pos(char * __restrict dst, int pos,
		const char * __restrict src)
{
	int len = strlen(src);
	memcpy(dst + pos, src, len + 1);

	return pos + len;
}

/// \warning, msg must contain the needed escape sequences!
static int build_msg_json(char *json, const char *chat_id, const char *msg,
		uint16_t len_max)
{
	int pos = 0;
	int len;

	// JSON format: {"chat_id":number,"text":"text_message"}
	pos = str_copy_pos(json, 0, "{\"chat_id\":");
	pos = str_copy_pos(json, pos, chat_id);
	pos = str_copy_pos(json, pos, ",\"text\":\"");
	// Truncate msg if it does not fit in buffer
	len = MIN(len_max - pos - 2 - 1, (int)strlen(msg));
	memcpy(json + pos, msg, len);
	pos += len;
	pos = str_copy_pos(json, pos, "\"}");

	return pos;
}

bool tgb_msg_send(const char *chat_id, const char *msg)
{
	char json[1024];
	int len;
	unsigned int recv_len = b.cmd_buf_len;
	int rc;

	len = build_msg_json(json, chat_id, msg, 1024);
	rc = tgb_post("sendMessage", json, len, "application/json",
			b.cmd_buf, &recv_len);

	if (rc < 200 || rc > 299) {
		return true;
	}
	/// TODO: tokenize and check if reply OK?
	return false;
}

