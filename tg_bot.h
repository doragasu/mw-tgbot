#ifndef _TG_BOT_H_
#define _TG_BOT_H_

#include <stdbool.h>
#define JSMN_STATIC
#include "mw/jsmn.h"

int tgb_init(char *cmd_buf, char *msg_buf, unsigned int cmd_buf_len,
		int msg_buf_len, const char *api_token);

/************************************************************************//**
 * \brief Generic GET request against the bot API, with specified method.
 *
 * \param[in]  method   Method for the bot API request.
 * \param[out] recv_buf Buffer used for HTTP response data.
 * \param[out] len      Length of the response.
 *
 * \return HTTP status code, or -1 if request was not completed.
 ****************************************************************************/
int tgb_get(const char *method, char *recv_buf, unsigned int *len);

/************************************************************************//**
 * \brief Generic POST request against the bot API, with specified method.
 *
 * On success, the obtained message is left in msg_buf provided during
 * initialization.
 *
 * \param[in]    method      Method with optional parameters when required.
 *               Must be null terminated. E.g. "sendPhoto".
 * \param[in]    data         Data to send in the POST body.
 * \param[in]    length       Length of the data to send.
 * \param[in]    content_type Content-Type HTTP header for the data payload.
 * \param[out]   recv_buf     Buffer used to receive reply data.
 * \param[inout] recv_len     On input, length of recv_buf. On output, length
 *                            of the received reply data.
 *
 * \return HTTP status code or -1 if the request was not completed.
 ****************************************************************************/
int tgb_post(const char *method, const char *data, int length,
		const char *content_type, char *recv_buf,
		unsigned int *recv_len);

/************************************************************************//**
 * \brief Checks the JSON obtained as a response to a Telegram method is OK.
 *
 * \param[in]  reply      Reply JSON. Must have null terminations added by
 *                        by calling json_null_terminate().
 * \param[in]  json_tok   Token array obtained from reply string.
 * \param[in]  num_tokens Number of tokens in token array.
 *
 * \return true if reply is OK, false otherwise.
 ****************************************************************************/
bool tgb_ok_check(const char *reply, const jsmntok_t *json_tok, int num_tokens);

/************************************************************************//**
 * \brief Send a message to specified chat.
 *
 * \param[in]    chat_id  Chat to which message will be sent.
 * \param[in]    msg      Message to send.
 * \param[out]   recv_buf Buffer used to receive reply data.
 * \param[inout] recv_len On input, length of recv_buf. On output, length
 *                        of the received reply data.
 *
 * \return false if message was properly sent, true otherwise.
 ****************************************************************************/
bool tgb_msg_send(const char *chat_id, const char *msg);

#endif /*_TG_BOT_H_*/

