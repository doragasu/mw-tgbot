# Simple Telegram Bot for MegaWiFi

## Introduction

This is a dumb Telegram bot to test MegaWiFi HTTPS API and JSON parser.

## Building

You will need a complete Genesis/Megadrive toolchain. The sources use some C standard library calls, such as `memcpy()`, `setjmp()`, `longjmp()`, etc. Thus your toolchain must include a C standard library implementation such as *newlib*.

You will also need your own Telegram bot API token. Open your favorite Telegram client and talk to @botfather to get one. Once you have one create the file `tg_token.h` and put the key there as in the following example:

```
#define TGB_API_TOKEN	"123456789:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
```

Then just run `make` to build the bot. Make sure you keep your API token private (e.g. do not upload it to public GitHub) as anyone having the token can control your bot.

## Running

Once built, burn the tg-bot file to your MegaWiFi cart. Also make sure firmware for the cart is version 1.1 or later. Previous versions lack the required HTTPS support.

Put the cart on the console and it will connect to the AP configured in the first slot. Then it will connect to the Telegram server, get updates and echo the received messages until console is turned off. Open your Telegram client and try saying something to the bot to see if it is working.

## Author

This bot has been written by Jesús Alonso (doragasu).

## Contributions

This project is just for testing and example purposes. No contributions are needed.

## License

This program is provided with NO WARRANTY, under the [Mozilla Public License (MPL)](https://www.mozilla.org/en-US/MPL/). It also includes jsmn JSON parser under the MIT license.

